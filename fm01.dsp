// License: cc0
// (Do whatever the f*** you want with it)

import("music.lib");
import("oscillator.lib");

// Control
freq = hslider("freq", 80, 80, 800, 1);
mod1f = hslider("mod1", 0.05, 0.01, 0.5, 0.01);
mod2f = hslider("mod2", 0.01, 0.01, 0.05, 0.01);
noisiness = hslider("noisiness", 0.01, 0.01, 32, 0.01);
vol = hslider("_gain", 0, 0, 1, 0.01);
rhy1 = hslider("rhy1", 0, 0, 1, 0.01);
rhy2 = hslider("rhy2", 0, 0, 1, 0.01);
rhy3 = hslider("rhy3", 0, 0, 1, 0.01);

rhythms = (1 - rhy1) + (lf_pulsetrainpos(2, 0.15) * rhy1)
        + (lf_pulsetrainpos(3, 0.25) * rhy2)
        + (lf_pulsetrainpos(7, 0.25) * rhy3);

mod1 = osc(freq * mod1f);
mod2 = osc(freq * mod2f);

process = osc(freq * mod1 * mod2 * (1 + (noise * noisiness))) *
    rhythms *
    vol <: _,_;
